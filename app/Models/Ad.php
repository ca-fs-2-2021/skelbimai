<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\City;

class Ad extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'author_id');
    }

    public function city()
    {
        return $this->hasOne(City::class,'id','city_id');
    }

    public function reType()
    {
        return $this->hasOne(ReType::class,'id','re_type');
    }
}
