<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ad;

class City extends Model
{
    use HasFactory;

    public function ads()
    {
        return $this->hasMany(Ad::class, 'city_id','id');
    }
}
