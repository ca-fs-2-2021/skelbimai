<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\City;
use App\Models\ReType;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $ads = Ad::all();
        return view('front.ads.list', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reTypes = ReType::all();
        $cities = City::all();
        return view('front.ads.create', compact('reTypes','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'address' => 'required|max:255',
            'description' => 'required',
            're_type' => 'required|integer',
            'author_id' => 'required|integer',
            'price' => 'required|numeric',
            'city_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return redirect('skelbimas/sukurti')
                ->withErrors($validator)
                ->withInput();
        }


        $title = $request->input('title');
        $ad = new Ad();
        $ad->title = $title;
        $ad->slug = Str::slug($title);
        $ad->description = $request->input('description');
        $ad->re_type = $request->input('re_type');
        $ad->city_id = $request->input('city');
        $ad->address = $request->input('address');
        $ad->author_id = Auth::user()->id;
        $ad->price =  $request->input('price');

        $ad->save();
        return redirect(route('all'));

    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show(string $slug)
    {
        $ad = Ad::where('slug','=',$slug)->first();
        return view('front.ads.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        //
    }

    public function ads()
    {
//        if(!Auth::check()){
//            return redirect(route('all'));
//        }
        $user = Auth::user();

        foreach ($user->ads as $ad)
        {
            echo $ad->title;
        }
    }

    public function byCity($id)
    {
        $city = City::find($id);
        foreach($city->ads as $ad){
            echo $ad->title;
        }
    }
}
