<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class Cities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cities() as $city) {
            DB::table('cities')->insert([
                'name' => $city
            ]);
        }
    }

    private function cities()
    {
        return [
            'Kauno raj.',
            'Vilniaus raj.',
            'Klaipėda',
            'Klaipėdos raj.',
            'Šiauliai',
            'Šiaulių raj.',
            'Panevėžys',
            'Panevėžio raj.',
            'Alytus',
            'Alytaus raj.',
            'Marijampolė',
            'Marijampolės raj.',
            'Akmenė',
            'Anykščiai',
            'Anykščių raj.',
            'Ariogala',
            'Aukštadvaris'
        ];
    }
}
