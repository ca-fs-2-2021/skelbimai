<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdsFornkeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->integer('re_type')->unsigned()->change();
            $table->foreign('re_type')->references('id')->on('re_types');
            $table->integer('city_id')->unsigned()->change();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->integer('author_id')->unsigned()->change();
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
            //
        });
    }
}
