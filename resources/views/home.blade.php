@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Home page') }}</div>

                <div class="card-body">
                    <div class="newest-ads">
                        <h2>Naujausi skelbimai</h2>
                        @foreach($newestAds as $ad)
                            <div class="">
                                <div class="title">
                                    <a href="{{route('skelbimas', $ad->slug)}}">
                                        {{$ad->title}}
                                    </a>
                                    <div class="price">
                                        {{$ad->price}}
                                    </div>
                                    <div class="date">
                                        {{$ad->updated_at}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
