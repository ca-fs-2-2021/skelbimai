<nav>
    <ul>
        <li>
            <a href="/">
                Pagrindinis
            </a>
        </li>
        <li>
            <a href="{{route('all')}}">
                Visi skelnimai
            </a>
        </li>
        <li>
            <a href="{{route('create_ad')}}">
                Sukurti skelbima
            </a>
        </li>
        @guest
            <li>
                <a href="register">
                    Registracija
                </a>
            </li>
        @endguest
        @auth
            <li>
                <a href="profile">
                    Mano profilis
                </a>
            </li>
        @endauth
        @if(date('m-d') == '11.03')
            <li>
                <a href="sales">
                    Ispardavimas
                </a>
            </li>
        @endif
    </ul>
</nav>
