@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @error('email')
            <span class="invalid-feedback" role="alert">
                erere
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Home page') }}</div>

                    <div class="card-body">
                        <div class="newest-ads">
                            <h2>Profilio informacija</h2>

                            <form action="{{route('user.update')}}" method="post">
                                @csrf
                                @method('post')
                                <input type="text" name="name" value="{{$user->name}}">
                                <input type="email" name="email" value="{{$user->email}}">
                                <input type="tel" name="phone" value="{{$user->phone}}">
                                <input type="submit" value="saugoti">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
