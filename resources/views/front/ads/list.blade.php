@extends('front.layout')

@section('content')
    <h1>
        Visi skelbimai
    </h1>
    @foreach($ads as $ad)
        <li>
            <a href="{{ route('skelbimas', $ad->slug) }}">
                {{$ad->title}}
            </a>
        </li>
    @endforeach
@endsection
