@extends('front.layout')

@section('content')
    <h1>
        Skelbimo sukurimas
    </h1>
    <form method="post" action="{{route('store_ad')}}">
        @csrf
        @method('POST')

        <input type="text" name="title">
        <input type="text" name="description">
        <input type="text" name="price">
        <input type="text" name="address">
        <select name="re_type">
            @foreach($reTypes as $type)
            <option value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
        </select>
        <select name="city">
            @foreach($cities as $city )
                <option value="{{$city->id}}">{{$city->name}}</option>
            @endforeach
        </select>
        <input type="submit" value="Sukurti">
    </form>
@endsection
