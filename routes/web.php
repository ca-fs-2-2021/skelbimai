<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\AdsController;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'index']);

Route::get('skelbimas/sukurti', [AdsController::class, 'create'])->name('create_ad')->middleware('auth');
Route::get('skelbimas/{slug}', [AdsController::class, 'show'])->name('skelbimas');
Route::get('visi-skelbimai', [AdsController::class, 'index'])->name('all');
Route::get('visi-skelbimai-pagal-miesta/{id}', [AdsController::class, 'byCity'])->name('all-by-city');
Route::get('visi-skelbimai-autoriaus', [AdsController::class, 'ads'])->name('all-users')->middleware('auth');

// to controller post method
Route::post('skelbimas/create', [AdsController::class, 'store'])->name('store_ad');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profilis', [UserController::class, 'edit'])->name('user.edit');
Route::post('/profilis', [UserController::class, 'update'])->name('user.update');


